**Current Version:** 1.0  
**Last Updated:** Feb. 7th, 2018

***You MUST be a Pro User on Roll 20 to create a Custom Character Sheet.***

* Navigate to http://www.roll20.net and login.
* Go your game and select it (don't launch).
* Click "Settings" then "Game Settings".
* Scroll down to "Character Sheet Template" and select "Custom" from the drop down.
* Copy the contents of "CharSheet.html" and "charsheet.css" to the respective locations.
* Save and launch your game, you can now create new character sheets and they will use this template.
 
Any issues or questions DM me on Discord (Simon#0005) or shoot me an e-mail (Simon@SimonRoth.net).


Thanks, 
Simon Roth